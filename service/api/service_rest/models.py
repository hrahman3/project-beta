from django.db import models
from django.urls import reverse

# Create your models here.

class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.employee_id

    def get_api_url(self):
        return reverse('api_technicians', kwargs={"id": self.id})


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=100)
    sold = models.BooleanField(default=False)


class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.TextField()
    vin = models.CharField(max_length=100)
    customer = models.CharField(max_length=100)
    vip = models.BooleanField(default=False)
    status = models.CharField(max_length=15, default="SCHEDULED")

    technician = models.ForeignKey(
        Technician,
        related_name='appointments',
        on_delete=models.PROTECT,
    )

    def __str__(self):
        return f"{self.vin} {self.status}"

    def get_api_url(self):
        return reverse('api_appointment', kwargs={'id': self.id})
