from django.urls import path

from .views import (
    api_technicians,
    api_technician,
    api_appointments,
    api_delete_appointment,
    api_cancel_appointment,
    api_finish_appointment,
    api_service_history
)


urlpatterns = [
    path("technicians/", api_technicians, name="api_technicians"),
    path("technicians/<int:id>/", api_technician, name="api_technician"),
    path("appointments/", api_appointments, name="api_appointments"),
    path(
        "appointments/<int:id>/", api_delete_appointment, name="api_delete_appointment"
    ),
    path(
        "appointments/<int:id>/cancel/",
        api_cancel_appointment,
        name="api_cancel_appointment",
    ),
    path(
        "appointments/<int:id>/finish/",
        api_finish_appointment,
        name="api_finish_appointment",
    ),
    path(
        "history/", api_service_history, name="api_service_history",
    )
]
