import { NavLink } from 'react-router-dom';


function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">Automersive</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
                <NavLink className="nav-link" aria-current="page" to="/">Home</NavLink>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="/" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">Technicians</a>
                <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                  <li className="nav-item">
                    <NavLink className="dropdown-item" aria-current="page" to="/technicians">Technicians</NavLink>
                  </li>
                  <li className="nav-item">
                      <NavLink className="dropdown-item" aria-current="page" to="/technicians/new">Add Technician</NavLink>
                  </li>
                </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="/" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">Appointments</a>
                <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                  <li className="nav-item">
                    <NavLink className="dropdown-item" aria-current="page" to="/appointments">Appointments</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="dropdown-item" aria-current="page" to="/appointments/new">Create an Appointment</NavLink>
                  </li>
                </ul>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="/history">Service History</NavLink>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="/" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">Vehicles</a>
                <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                  <li className="nav-item">
                    <NavLink className="dropdown-item" aria-current="page" to="/models/new">Add a Model</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="dropdown-item" aria-current="page" to="/automobiles">Automobiles</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="dropdown-item" aria-current="page" to="/automobiles/new">Add an Automobile</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="dropdown-item" to="/manufacturers">Manufacturers</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="dropdown-item" to="/manufacturers/create">Create a Manufacturer</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="dropdown-item" to="/models">Models</NavLink>
                  </li>
                </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="/" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">Sales</a>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                  <li className="nav-item">
                    <NavLink className="dropdown-item" to="/sales">Sales</NavLink>
                  </li>
                  <li className="nav-item">
                      <NavLink className="dropdown-item" aria-current="page" to="/sales/new">Record a new Sale</NavLink>
                    </li>
                  <li className="nav-item">
                    <NavLink className="dropdown-item" to="/salespeople">Salespeople</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="dropdown-item" to="/salespeople/add">Add a Salesperson</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="dropdown-item" to="/sales/history">Salesperson History</NavLink>
                  </li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="/" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">Customers</a>
                <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                  <li className="nav-item">
                    <NavLink className="dropdown-item" to="/customers">Customers</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="dropdown-item" to="/customers/add">Add a Customer</NavLink>
                  </li>
                </ul>
            </li>
          </ul>
          <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search"/>
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
          </form>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
